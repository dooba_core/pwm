/* Dooba SDK
 * Pulse Width Modulation
 */

#ifndef	__PWM_HEARTBEAT_H
#define	__PWM_HEARTBEAT_H

// External Includes
#include <stdint.h>

// States
#define	PWM_HEARTBEAT_STATE_OFF					0
#define	PWM_HEARTBEAT_STATE_INC					1
#define	PWM_HEARTBEAT_STATE_ON					2
#define	PWM_HEARTBEAT_STATE_DEC					3

// Heartbeat structure
struct pwm_heartbeat
{
	// Pin
	uint8_t pin;

	// On Time (cycles)
	uint32_t on_time;

	// On Value
	uint8_t on_val;

	// Off Time (cycles)
	uint32_t off_time;

	// Off Value
	uint8_t off_val;

	// Speed divider (Cycles per increment)
	uint32_t cpi;

	// State
	uint8_t state;

	// Timer
	uint32_t timer;

	// Current Value
	uint8_t val;
};

// Initialize Heartbeat
extern void pwm_heartbeat_init(struct pwm_heartbeat *h, uint8_t pin, uint32_t on_time, uint8_t on_val, uint32_t off_time, uint8_t off_val, uint32_t speed_div);

// Update Heartbeat
extern void pwm_heartbeat_update(struct pwm_heartbeat *h);

#endif
