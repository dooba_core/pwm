/* Dooba SDK
 * Pulse Width Modulation
 */

// External Includes
#include <stdlib.h>
#include <avr/io.h>

// Internal Includes
#include "pwm.h"

// Set PWM on pin
void pwm_set(uint8_t pin, uint8_t v)
{
	// Check Value
	if(v == 0)																			{ pwm_off(pin); return; }

	// Check Pin { Set 8-Bit Fast PWM mode with no prescaler }
	switch(pin)
	{
		// Pin 0
		case PWM_PIN_0:
			DDRD |= _BV(4);
			PORTD &= ~(_BV(4));
			TCCR1A |= _BV(WGM10) | _BV(COM1B1);
			TCCR1B |= _BV(WGM12) | _BV(CS10);
			OCR1B = v;
			break;

		// Pin 1
		case PWM_PIN_1:
			DDRD |= _BV(5);
			PORTD &= ~(_BV(5));
			TCCR1A |= _BV(WGM10) | _BV(COM1A1);
			TCCR1B |= _BV(WGM12) | _BV(CS10);
			OCR1A = v;
			break;

		// Pin 2
		case PWM_PIN_2:
			DDRD |= _BV(6);
			PORTD &= ~(_BV(6));
			TCCR2A |= _BV(WGM20) | _BV(WGM21) | _BV(COM2B1);
			TCCR2B |= _BV(CS20);
			OCR2B = v;
			break;

		// Pin 3
		case PWM_PIN_3:
			DDRD |= _BV(7);
			PORTD &= ~(_BV(7));
			TCCR2A |= _BV(WGM20) | _BV(WGM21) | _BV(COM2A1);
			TCCR2B |= _BV(CS20);
			OCR2A = v;
			break;

		// Pin 4
		case PWM_PIN_4:
			DDRB |= _BV(3);
			PORTB &= ~(_BV(3));
			TCCR0A |= _BV(WGM00) | _BV(WGM01) | _BV(COM0A1);
			TCCR0B |= _BV(CS00);
			OCR0A = v;
			break;

		// Pin 5
		case PWM_PIN_5:
			DDRB |= _BV(7);
			PORTB &= ~(_BV(7));
			TCCR3A |= _BV(WGM30) | _BV(COM3B1);
			TCCR3B |= _BV(WGM32) | _BV(CS30);
			OCR3B = v;
			break;

		// Pin 6
		case PWM_PIN_6:
			DDRB |= _BV(6);
			PORTB &= ~(_BV(6));
			TCCR3A |= _BV(WGM30) | _BV(COM3A1);
			TCCR3B |= _BV(WGM32) | _BV(CS30);
			OCR3A = v;
			break;

		// Pin 7
		case PWM_PIN_7:
			DDRB |= _BV(4);
			PORTB &= ~(_BV(4));
			TCCR0A |= _BV(WGM00) | _BV(WGM01) | _BV(COM0B1);
			TCCR0B |= _BV(CS00);
			OCR0B = v;
			break;

		// Default
		default:
			break;
	}
}

// Disable PWM on pin
void pwm_off(uint8_t pin)
{
	// Check Pin { Disable PWM }
	switch(pin)
	{
		// Pin 0
		case PWM_PIN_0:
			DDRD |= _BV(4);
			PORTD &= ~(_BV(4));
			TCCR1A &= ~(_BV(COM1B1));
			break;

		// Pin 1
		case PWM_PIN_1:
			DDRD |= _BV(5);
			PORTD &= ~(_BV(5));
			TCCR1A &= ~(_BV(COM1A1));
			break;

		// Pin 2
		case PWM_PIN_2:
			DDRD |= _BV(6);
			PORTD &= ~(_BV(6));
			TCCR2A &= ~(_BV(COM2B1));
			break;

		// Pin 3
		case PWM_PIN_3:
			DDRD |= _BV(7);
			PORTD &= ~(_BV(7));
			TCCR2A &= ~(_BV(COM2A1));
			break;

		// Pin 4
		case PWM_PIN_4:
			DDRB |= _BV(3);
			PORTB &= ~(_BV(3));
			TCCR0A &= ~(_BV(COM0A1));
			break;

		// Pin 5
		case PWM_PIN_5:
			DDRB |= _BV(7);
			PORTB &= ~(_BV(7));
			TCCR3A &= ~(_BV(COM3B1));
			break;

		// Pin 6
		case PWM_PIN_6:
			DDRB |= _BV(6);
			PORTB &= ~(_BV(6));
			TCCR3A &= ~(_BV(COM3A1));
			break;

		// Pin 7
		case PWM_PIN_7:
			DDRB |= _BV(4);
			PORTB &= ~(_BV(4));
			TCCR0A &= ~(_BV(COM0B1));
			break;

		// Default
		default:
			break;
	}
}
