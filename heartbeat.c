/* Dooba SDK
 * Pulse Width Modulation
 */

// Internal Includes
#include "pwm.h"
#include "heartbeat.h"

// Initialize Heartbeat
void pwm_heartbeat_init(struct pwm_heartbeat *h, uint8_t pin, uint32_t on_time, uint8_t on_val, uint32_t off_time, uint8_t off_val, uint32_t speed_div)
{
	// Setup
	h->pin = pin;
	h->on_time = on_time;
	h->on_val = on_val;
	h->off_time = off_time;
	h->off_val = off_val;
	h->cpi = speed_div ? speed_div : 1;
	h->state = PWM_HEARTBEAT_STATE_INC;
	h->timer = h->cpi;
	h->val = h->off_val;
	pwm_set(h->pin, h->off_val);
}

// Update Heartbeat
void pwm_heartbeat_update(struct pwm_heartbeat *h)
{
	// Dec Timer
	h->timer = h->timer - 1;
	if(h->timer)															{ return; }

	// Update
	if(h->state == PWM_HEARTBEAT_STATE_OFF)
	{
		// Go to inc
		h->state = PWM_HEARTBEAT_STATE_INC;
		h->timer = h->cpi;
		return;
	}
	else if(h->state == PWM_HEARTBEAT_STATE_INC)
	{
		// Inc
		h->val = h->val + 1;
		if(h->val == h->on_val)												{ h->state = PWM_HEARTBEAT_STATE_ON; h->timer = h->on_time; }
		else																{ h->timer = h->cpi; }
	}
	else if(h->state == PWM_HEARTBEAT_STATE_ON)
	{
		// Go to dec
		h->state = PWM_HEARTBEAT_STATE_DEC;
		h->timer = h->cpi;
		return;
	}
	else if(h->state == PWM_HEARTBEAT_STATE_DEC)
	{
		// Dec
		h->val = h->val - 1;
		if(h->val == h->off_val)											{ h->state = PWM_HEARTBEAT_STATE_OFF; h->timer = h->off_time; }
		else																{ h->timer = h->cpi; }
	}
	else																	{ return; }

	// Set new value
	pwm_set(h->pin, h->val);
}
