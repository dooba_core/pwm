/* Dooba SDK
 * Pulse Width Modulation
 */

#ifndef	__PWM_H
#define	__PWM_H

// External Includes
#include <stdint.h>

// PWM-enabled Pins
#define	PWM_PIN_0							18
#define	PWM_PIN_1							19
#define	PWM_PIN_2							20
#define	PWM_PIN_3							21
#define	PWM_PIN_4							11
#define	PWM_PIN_5							15
#define	PWM_PIN_6							14
#define	PWM_PIN_7							12

// Set PWM on pin
extern void pwm_set(uint8_t pin, uint8_t v);

// Turn off PWM on pin
extern void pwm_off(uint8_t pin);

#endif
