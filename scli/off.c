/* Dooba SDK
 * Pulse Width Modulation
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "pwm.h"
#include "scli/off.h"

// Set PWM Off
void pwm_scli_off(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	uint8_t p;

	// Acquire Pin Number
	if(str_next_arg(args, args_len, &x, &l) == 0)					{ scli_printf("Please provide a valid pin number\n"); return; }
	p = strtoul(x, 0, 0);

	// Set PWM Off
	scli_printf("Disabling PWM on pin [%i]...\n", p);
	pwm_off(p);
}
