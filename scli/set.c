/* Dooba SDK
 * Pulse Width Modulation
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "pwm.h"
#include "scli/set.h"

// Set PWM
void pwm_scli_set(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	uint8_t p;
	uint8_t v;

	// Acquire Pin Number & Value
	if(str_next_arg(args, args_len, &x, &l) == 0)					{ scli_printf("Please provide a valid pin number\n"); return; }
	p = strtoul(x, 0, 0);
	if(str_next_arg(args, args_len, &x, &l) == 0)					{ scli_printf("Please provide a valid PWM value (0-255)\n"); return; }
	v = strtoul(x, 0, 0);

	// Set PWM
	scli_printf("Setting pin [%i] as PWM [%i]...\n", p, v);
	pwm_set(p, v);
}
