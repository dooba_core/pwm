/* Dooba SDK
 * Pulse Width Modulation
 */

#ifndef	__PWM_SCLI_OFF_H
#define	__PWM_SCLI_OFF_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Set PWM Off
extern void pwm_scli_off(char **args, uint16_t *args_len);

#endif
