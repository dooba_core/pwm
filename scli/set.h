/* Dooba SDK
 * Pulse Width Modulation
 */

#ifndef	__PWM_SCLI_SET_H
#define	__PWM_SCLI_SET_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Set PWM
extern void pwm_scli_set(char **args, uint16_t *args_len);

#endif
